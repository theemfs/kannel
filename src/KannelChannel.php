<?php

namespace NZX\NotificationChannels\Kannel;

use Illuminate\Notifications\Notification;

class KannelChannel
{
    protected $KannelHttp;

    public function __construct(KannelHttp $KannelHttp)
    {
        $this->KannelHttp = $KannelHttp;
    }

    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor('kannel')) {
            return;
        }

        $kannel_out_message = $notification->toKannel($notifiable);

        return $this
            ->KannelHttp
            ->send(
                $kannel_out_message->getTo(),
                $kannel_out_message->getText(),
                $kannel_out_message->getDlrurl()
            );
    }
}
