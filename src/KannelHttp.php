<?php

namespace NZX\NotificationChannels\Kannel;

use GuzzleHttp\Client;

class KannelHttp
{
    public function __construct($url, $user, $password)
    {
        $this->url = $url;
        $this->user = $user;
        $this->password = $password;
    }

    public function send($to, $text, $dlrurl = null)
    {
        if ($dlrurl) {
            $send_url = "$this->url?username=$this->user&password=$this->password&coding=2&dlr-url=$dlrurl&dlr-mask=7&to=$to&text=$text";
        } else {
            $send_url = "$this->url?username=$this->user&password=$this->password&coding=2&to=$to&text=$text";
        }

        if (':test_url:' == $this->url) {
            return "0. Accepted to delivery";
        }

        $client = new Client();

        return $client->request('GET', $send_url);
        // return file_get_contents($send_url); //todo: use guzzle or curl
    }

    // from=%p&
        // text=%a&
        // char=%C&
        // code=%c&
        // time=%T&
        // first_word=%k&
        // second_word=%s&
        // second_word2=%S&
        // other_words=%r&
        // original_binary_url_encoded=%b&
        // original_binary_hex=%e&
        // time_formatted=%t&
        // to=%P&
        // from_with_plus=%q&
        // to_with_plus=%Q&
        // smsc_id=%i&
        // smsc_id_internal=%I&
        // delivery_report=%d&
        // delivery_report_url=%R&
        // delivery_report_message=%N&
        // meta_data=%D&
        // delivery_report_reply=%A&
        // foreign_message_id=%F&
        // sendsms_user=%n&
        // message_class=%m&
        // mwi=%M&
        // udh=%u&
        // billing_id=%B&
        // billing_id2=%o&
        // dcs=%O&
        // originating_smsc=%f&
        // smsbox_id=%x&
        // validity_period=%v&
        // deferred=%V&
}
