<?php

namespace NZX\NotificationChannels\Kannel;

class KannelInMessage extends KannelSms
{
    protected $rowText;

    public function __construct($data)
    {
        if (array_key_exists('text', $data)) {
            $this->rowText = $data['text'];
        }

        if (array_key_exists('from', $data)) {
            $this->setFrom($data['from']);
        }

        if (array_key_exists('char', $data)) {
            $this->setChar($data['char']);
        }

        if (array_key_exists('code', $data)) {
            $this->setCode($data['code']);
        }

        if (array_key_exists('time', $data)) {
            $this->setTime($data['time']);
        }

        if (array_key_exists('smsc', $data)) {
            $this->setSmsc($data['smsc']);
        }

        $this->setText($this->rowText);
    }

    public function setText($text)
    {
        $this->text = $this->decode($text, $this->code, $this->char);

        return $this;
    }
}
