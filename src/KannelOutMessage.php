<?php

namespace NZX\NotificationChannels\Kannel;

class KannelOutMessage extends KannelSms
{
    public function __construct($data)
    {
        if (array_key_exists('text', $data)) {
            $this->setText($data['text']);
        }

        if (array_key_exists('to', $data)) {
            $this->setTo($data['to']);
        }

        if (array_key_exists('dlrurl', $data)) {
            $this->setDlrurl($data['dlrurl']);
        }
    }

    public function setText($text)
    {
        $this->text = $this->encode($text);

        return $this;
    }
}
