<?php

namespace NZX\NotificationChannels\Kannel;

use Illuminate\Support\ServiceProvider;

class KannelServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->when(KannelChannel::class)
            ->needs(KannelHttp::class)
            ->give(function () {
                $config = config('services.kannel');

                return new KannelHttp(
                    $config['url'],
                    $config['user'],
                    $config['password']
                );
            });
    }
}
