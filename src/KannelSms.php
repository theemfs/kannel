<?php

namespace NZX\NotificationChannels\Kannel;

use NZX\NotificationChannels\Kannel\Traits\KannelHelpers;

class KannelSms
{
    use KannelHelpers;

    /** @var string */
    protected $to;
    protected $from;
    protected $text;
    protected $char;
    protected $code;
    protected $time;
    protected $smsc;
    protected $dlrurl;

    public static function parse(array $data)
    {
        return new static($data);
    }

    public static function create($data)
    {
        return new static($data);
    }

    // public function __construct($data)
    // {
    //     // general
    //     $this->text = array_key_exists('text', $data['text']) ? urlencode(iconv("utf-8", "ucs-2be", $text)) : null;

    //     // outgoing
    //     $this->from = array_key_exists('from', $data['from']) ?? null;
    //     $this->dlrurl = array_key_exists('dlrurl', $data['dlrurl']) ?? null;

    //     // inbound
    //     $this->to = array_key_exists('to', $data['to']) ?? null;
    //     $this->char = array_key_exists('char', $data['char']) ?? null;
    //     $this->code = array_key_exists('code', $data['code']) ?? null;
    //     $this->time = array_key_exists('time', $data['time']) ?? null;
    //     $this->smsc = array_key_exists('smsc', $data['smsc']) ?? null;
    // }

    public function setTo($to)
    {
        $this->to = static::filter($to);

        return $this;
    }

    public function setFrom($from)
    {
        $number = static::filter($from);

        if (11==mb_strlen($number)) {
            $this->from = $number;
        } else {
            $this->from = $from;
        }

        return $this;
    }

    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    public function setChar($char)
    {
        $this->char = $char;

        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    public function setSmsc($smsc)
    {
        $this->smsc = $smsc;

        return $this;
    }

    public function setDlrurl($dlrurl)
    {
        $this->dlrurl = $dlrurl;

        return $this;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getChar()
    {
        return $this->char;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function getSmsc()
    {
        return $this->smsc;
    }

    public function getDlrurl()
    {
        return $this->dlrurl;
    }
}
