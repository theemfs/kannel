<?php

namespace NZX\NotificationChannels\Kannel\Traits;

trait KannelHelpers
{
    public static function encode(string $text)
    {
        return urlencode(mb_convert_encoding($text, "ucs-2be", "utf-8")); //ucs-4be not supported on some phones
    }

    public static function decode(string $text, $code, $char)
    {
        if ("0" == $code) {
            return $text;
        }

        if ("2" == $code) {
            if ("UTF-8" == $char) {
                return urldecode($text);
            };
            if ("UTF-16BE" == $char) {
                return mb_convert_encoding(urldecode($text), "utf-8", "ucs-2be");
            }
        }
    }

    public static function filter(string $number, $prefix = '7')
    {
        $number = preg_replace("/[^0-9]/", '', $number);

        if (11 == mb_strlen($number)) {
            $number[0] = $prefix;
        }

        return $number;
    }
}
